<?php

namespace App\Http\Controllers;

use Auth;
use App\Job;
use App\User;
use Illuminate\Http\Request;
use App\Services\ProfileService;

class ProfileController extends Controller
{

    protected $profileService;

    public function __construct(ProfileService $profileService)
    {
        $this->profileService = $profileService;
    }

    public function index()
    {
        $user = Auth::user();

        return view('profile.index', compact('user'));
    }

    public function create()
    {
        $user = Auth::user();
        return view('profile.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->profileService->addProfile($request, Auth::user());

        flash('Profile updated successfully')->success();
        return redirect()->route('profile.showinterests');
    }

    public function interests()
    {
        $jobs = Job::pluck('title', 'id');
        return view('profile.interests', compact('jobs'));
    }

    public function saveInterests(Request $request)
    {
        $this->profileService->addInterests($request);
        return redirect()->route('profile.showExperience');
    }

    public function experience()
    {
        $jobs = $this->profileService->getUserJobs();

        return view('profile.experience', compact('jobs'));
    }

    public function saveExperience(Request $request)
    {
        $this->profileService->saveExperience($request);

        return redirect()->route('profile.showFreelancer');
    }

    public function freelancer()
    {
        return view('profile.freelancer');
    }

    public function saveFreelancer(Request $request)
    {
        $this->profileService->saveFreelancer($request);

        flash('Your request is awaiting approval')->success();
        return redirect()->route('profile.index');
    }

}
