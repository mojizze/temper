<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Services\ChartService;

class HomeController extends Controller
{
    protected $userService;
    protected $chartService;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(UserService $userService, ChartService $chartService)
    {
        $this->userService= $userService;
        $this->chartService= $chartService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 'admin') {
            return redirect()->route('dashboard');
        }

        return redirect()->route('profile.index');
    }

    public function homepage()
    {
        return view('home');
    }

    public function chartData()
    {
        $chart_users = $this->userService->getChartUsers();
        $chart_data = $this->chartService->getTrend($chart_users);

        return response()->json([
            'error' => null,
            'data' => $chart_data
        ], 200);

    }

    public function dashboard ()
    {
        return view('dashboard');
    }

}
