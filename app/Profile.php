<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    protected $fillable = [
        'user_id', 'title', 'phone', 'dob', 'postcode', 'address', 'additional'
    ];

    public function user()
    {
        return $this->belongsTo(User::Class);
    }
}
