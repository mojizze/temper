<?php

namespace App\Services;

use Excel;
use App\User;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;

class UserService
{
    public function getAllUsers()
    {
        $users = User::where('role', 'user');

        return $users;
    }

    public function getChartUsers()
    {
        $users = User::where('role', 'user')
            ->select('onboarding_percentage', 'created_at', 'id')
            ->get()
            ->toArray();

        return $users;
    }
}
