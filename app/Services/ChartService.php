<?php

namespace App\Services;

use DB;
use Excel;
use App\User;

class ChartService
{
    public function getOnboardingTrend()
    {
        $data = DB::select("
        SELECT onboarding_percentage, COUNT(onboarding_percentage), WEEKOFYEAR(created_at)
            FROM users
            WHERE role = 'user'
            GROUP BY onboarding_percentage, WEEKOFYEAR(created_at);
        ");

        return $data;
    }

    public function getTrend($users)
    {
        $data = [];


        foreach ($users as $row) {
            if( array_check($row['created_at'], $data)
    		 	&& array_check($row['onboarding_percentage'], $data[$row['created_at']]) )
    		{
    		 	$data[$row['created_at']][$row['onboarding_percentage']] += 1;
    		}else {
    		 	$data[$row['created_at']][$row['onboarding_percentage']] = 1;
    		}
        }

        return $data;
    }
}
