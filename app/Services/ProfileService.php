<?php

namespace App\Services;

use Auth;
use App\User;
use App\Profile;
use Carbon\Carbon;
use Spatie\Permission\Models\Role;

class ProfileService
{
    public function addProfile($request, $user)
    {
        $rd = $request->all();
        $rd['user_id'] = $user->id;

        if (!count($user->profile)) {
            Profile::create($rd);
            $user->update(['onboarding_percentage' => 40]);
        }

        //Profile::update($rd);
    }

    public function addInterests($request)
    {
        $rd = $request->all();
        $user = Auth::user();
        $user->jobs()->sync($rd['job_id']);
        $user->update(['onboarding_percentage' => 50]);
    }

    public function getUserJobs()
    {
        $jobs = Auth::user()->jobs()->get();

        return $jobs;
    }

    public function saveExperience($request)
    {
        $rd = $request->all();
        $user = Auth::user();

        if ($rd['experience'] == 'yes') {
            $user->update(['experience' => true]);
        }

        $user->update(['onboarding_percentage' => 70]);

    }

    public function saveFreelancer($request)
    {
        $rd = $request->all();
        $user = Auth::user();

        if ($rd['freelancer'] == 'yes') {
            $user->update(['freelancer' => true]);
        }

        $user->update(['onboarding_percentage' => 90]);
    }
}
