<?php

function date_f($str_date) {
    $parts = explode('/', $str_date);
    $date = $parts[2] . '-' . $parts[1] . '-' . $parts[0];

    return $date;
}

function array_check($item , $array)
{
    return preg_match('/"'.$item.'"/i' , json_encode($array));
}
