<?php


use App\User;
use Carbon\Carbon;
use App\Application;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        //Create Admin
        User::create([
            'name' => 'John Doe',
            'email' => 'john@admin.com',
            'password' => bcrypt('secret'),
            'created_at' => '2017-07-19',
            'role' => 'admin',
            'activated' => true,
        ]);



        // Import other users
        $path = storage_path('files/temper_users.csv');

		$data = Excel::load($path, function($reader) {})->get();

		if(!empty($data) && $data->count()){
            $users = [];
			foreach ($data->toArray() as $key => $value) {
				if(!empty($value)){
                    $value['name'] = $faker->name;
                    $value['password'] = bcrypt(str_random(8));
                    $value['email'] = $faker->email;
                    $value['role'] = 'user';
                    $value['created_at'] = date_f($value['created_at']);
                    $value['activated'] = true;

                    array_push($users, $value);
				}
			}

            User::insert($users);
		}
    }
}
