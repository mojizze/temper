<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jobs = [
            ['title' => 'Web Developer'],
            ['title' => 'UI/UX Developer'],
            ['title' => 'Graphics Designer'],
            ['title' => 'Buisness Developer'],
            ['title' => 'Software Tester'],
        ];

        foreach ($jobs as $job) {
            App\Job::create($job);
        }
    }
}
