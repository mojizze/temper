@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Who are You?</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('profile.saveFreelancer') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('freelancer') ? ' has-error' : '' }}">
                            <label for="freelancer" class="col-md-4 control-label">Are you a freelancer?</label>

                            <div class="col-md-6">
                                {!!  Form::select('freelancer', ['yes' => 'yes', 'no' => 'no'], null, ['class' => 'form-control']) !!}

                                @if ($errors->has('freelancer'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('freelancer') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Info
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
