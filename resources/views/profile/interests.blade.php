@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Who are You?</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('profile.saveInterests') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('job_id[]') ? ' has-error' : '' }}">
                            <label for="job_id[]" class="col-md-4 control-label">What jobs are you interested in?</label>

                            <div class="col-md-6">
                                {!!  Form::select('job_id[]', $jobs, null, ['class' => 'form-control multi-select', 'multiple' => 'multiple']) !!}

                                @if ($errors->has('job_id[]'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('job_id[]') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Info
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
