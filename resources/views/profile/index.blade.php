@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Your Profile</div>

                <div class="panel-body">
                    <p>
                        <strong>Name: </strong> {{ $user->name }}
                    </p>
                    <p>
                        <strong>Email: </strong> {{ $user->email }}
                    </p>
                    <p>
                        <strong>Role: </strong> {{ $user->role }}
                    </p>
                    <p>
                        <strong>Onboarding Percentage: </strong> {{ $user->onboarding_percentage }}&#37;
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
