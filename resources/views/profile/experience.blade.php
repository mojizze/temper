@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Who are You?</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('profile.saveExperience') }}">
                        {{ csrf_field() }}

                        <div class="col-md-6 col-md-offset-3">
                            <h4>Chosen Jobs</h4>
                            <ul>
                                @foreach ($jobs as $job)
                                    <li>{{ $job->title }}</li>
                                @endforeach
                            </ul>
                        </div>
                        <br>

                        <div class="form-group{{ $errors->has('experience') ? ' has-error' : '' }}">
                            <label for="experience" class="col-md-4 control-label">Do You have relevant experience in these jobs?</label>

                            <div class="col-md-6">
                                {!!  Form::select('experience', ['yes' => 'yes', 'no' => 'no'], null, ['class' => 'form-control']) !!}

                                @if ($errors->has('experience'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('experience') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Info
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
