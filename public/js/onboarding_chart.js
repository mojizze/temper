Vue.use(VueResource);

var app = new Vue({
    el: '#app',
    data: {},
    methods: {
        displayError: function() {
            alert("An error occured");
        },
        fetchData: function() {
            this.$http.get('/chart-data')
                .then(function(response) {
                    var CordinateList = [];

                    $.each(response.body.data, function(key, value) {
                        var cordinates = [];

                        $.each(value, function(key, value) {
                            cordinates.push([parseInt(key), value]);
                        })

                        var cordinateObeject = {
                            name: key,
                            data: cordinates
                        }
                        CordinateList.push(cordinateObeject);
                    });


                    Highcharts.chart('chartDiv', {
                        chart: {
                            type: 'spline',
                            inverted: false
                        },
                        title: {
                            text: 'Retention curve'
                        },
                        xAxis: {
                            reversed: false,
                            title: {
                                enabled: true,
                                text: 'Onboarding steps'
                            },

                            showLastLabel: true
                        },
                        yAxis: {
                            title: {
                                text: 'Users percentage'
                            }
                        },
                        legend: {
                            enabled: false
                        },
                        plotOptions: {
                            spline: {
                                marker: {
                                    enable: false
                                }
                            }
                        },
                        series: CordinateList
                    });
                }, function(response) {
                    this.displayError();
                });
        }
    }
})

app.fetchData();
