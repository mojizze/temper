<?php

namespace Tests\Feature;

use App\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BasicTest extends TestCase
{
    /**
    * Test if home redirects to dashboard.
    */
    public function testHomeRedirect()
    {
        $uri = '/dashboard';
        $response = $this->get('/');

        $response->assertRedirect($uri);
    }


    /**
    * Test dashboard page.
    */
    public function testDashboard()
    {
        $response = $this->get('/dashboard');

        $response->assertStatus(200);
    }

    /**
    * Test the API endpoing for the chart json.
    */
    public function testChartJson()
    {
        $response = $this->withHeaders([
            'Content-Type' => 'application/json',
        ])->json('GET', '/chart-data');

        $response->assertStatus(200);
    }

    /**
    * Test if users have been populated.
    * NOTE: This uses the live database and not the teste db.
    */
    public function testUsersCount()
    {
        $users_count = User::where('role', 'user')
                        ->count();
        $count_test = $users_count >= 339;

        $this->assertTrue($count_test);
    }
}
