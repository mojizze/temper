<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@dashboard')->name('dashboard');
Route::get('/chart-data', 'HomeController@chartData')->name('chartData');

Route::get('user/activation/{token}', 'Auth\LoginController@activateUser')->name('user.activate');


Route::middleware(['auth'])->group(function () {
    Route::resource('profile', 'ProfileController');
    Route::get('profile/jobs/interests', 'ProfileController@interests')->name('profile.showinterests');
    Route::post('profile/jobs/interests', 'ProfileController@saveInterests')->name('profile.saveInterests');
    Route::get('profile/jobs/experience', 'ProfileController@experience')->name('profile.showExperience');
    Route::post('profile/jobs/experience', 'ProfileController@saveExperience')->name('profile.saveExperience');
    Route::get('profile/jobs/freelancer', 'ProfileController@freelancer')->name('profile.showFreelancer');
    Route::post('profile/jobs/freelancer', 'ProfileController@saveFreelancer')->name('profile.saveFreelancer');

    Route::resource('users', 'UsersController');
    Route::get('approve-user/{id}', 'UsersController@approveUser')->name('users.approve');
});

Route::get('foo', function() {

    $users = App\User::where('role', 'user')
        ->select('onboarding_percentage', 'created_at', 'id')
        ->get()
        ->toArray();

    $data = array();


    foreach ($users as $row) {
        if( array_check($row['created_at'], $data)
		 	&& array_check($row['onboarding_percentage'], $data[$row['created_at']]) )
		{
		 	$data[$row['created_at']][$row['onboarding_percentage']] += 1;
		}else {
		 	$data[$row['created_at']][$row['onboarding_percentage']] = 1;
		}
    }



    dd($data);
});
