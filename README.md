# Temper

## Installation
- clone the repository
- open the repository folder in your command line tool
- make a copy of the environment file: `cp .env.example .env`
- install packages by running the command: `composer install`
- generate encryption key: `php artisan key:generate`
- update the `.env` file with preferred database and mail credentials
- run migration and seed: `php artisan:migrate --seed` (this mat take some time)
- run the app on a php server: `php artisan serve`
- navigate to the url displayed on your browser to view the application

**NOTE:** If you have an exisisting database, run the command `php artisan migrate:fresh` before proceeding

## Tests
`./vendor/bin/phpunit`


## User Details (Not needed)

To login as admin, use the credentials below;

**email**: john@admin.com

**password:** secret

Thanks.
